package ropay.service.soap;

import org.apache.cxf.annotations.SchemaValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ropay.database.interfaces.UserDao;
import ropay.database.objects.User;

import javax.jws.WebService;

@Component
@WebService(endpointInterface="ropay.service.soap.UserService")
@SchemaValidation(type = SchemaValidation.SchemaValidationType.BOTH)
public class UserServiceImpl implements UserService {

    public UserServiceImpl() {
    }

    @Autowired
    private UserDao userDao;

    @Override
    public User getUser(String login) {
        return userDao.findByLogin(login);
    }

    @Override
    public String createUser(User user) {
        userDao.create(user);
        return "OK";
    }
}
