package ropay.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ropay.database.interfaces.UserDao;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping(value = "/userList")
public class UserListController {

    private Logger logger = LoggerFactory.getLogger(UserListController.class);

    @Autowired
    private UserDao userDao;

    @RequestMapping(method = GET)
    public String getUserList(Model model) {
        logger.trace("Get user list");
        model.addAttribute("userList", userDao.findAll());
        return "home";
    }

}
