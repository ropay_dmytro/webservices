package ropay.database.dao;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ropay.database.interfaces.UserDao;
import ropay.database.objects.User;

import java.util.List;

@Repository("userDao")
public class UserDaoImpl extends HibernateDaoImpl implements UserDao {

    private static Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    @Override
    @Transactional
    public void create(User user) {
        logger.trace("Create user: " + user);
        User newUser = findByLogin(user.getLogin());
        if (newUser == null) {
            try {
                user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
                insert(user);
            } catch (HibernateException e) {
                logger.error("Create user exception " + e, e);
                throw e;
            }
        } else {
            logger.error("That user exist");
            throw new IllegalArgumentException("This user exists");
        }
    }

    @Override
    @Transactional
    public void update(User user) {
        logger.trace("Update user: " + user);
        User newUser = findByLogin(user.getLogin());
        if (newUser != null) {
            user.setId(newUser.getId());
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            try {
                updateObject(user);
            } catch (HibernateException e) {
                logger.error("Update user exception " + e, e);
                throw e;
            }
        } else {
            logger.error("That user doesn't exist");
            throw new IllegalArgumentException("That user doesn't exist");
        }
    }

    @Override
    @Transactional
    public void remove(User user) {
        logger.trace("Remove user: " + user);
        User newUser = findByLogin(user.getLogin());
        if (newUser != null) {
            try {
                deleteObject(newUser);
            } catch (HibernateException e) {
                logger.error("Delete user exception " + e, e);
                throw e;
            }
        } else {
            logger.error("That user doesn't exist");
            throw new IllegalArgumentException("That role doesn't exist");
        }
    }

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        logger.trace("Find all users");
        String query = "from User";
        try {
            List<User> result = findAllRecords(query);
            return result;
        } catch (HibernateException e) {
            logger.error("Find all users exception " + e, e);
            throw e;
        }
    }

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public User findByLogin(String login) {
        logger.trace("Find by login: " + login);
        try {
            String query = "from User where login = :key";
            List<User> result = findByQuery(query, login);
            if (result.size() != 0) {
                return result.get(0);
            } else {
                logger.trace("Thar user doesn't exist");
                return null;
            }
        } catch (HibernateException e) {
            logger.error("Find by login user exception " + e, e);
            throw e;
        }
    }

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public User findByEmail(String email) {
        logger.trace("Find by email: " + email);
        String query = "from User where email = :key";
        try {
            List<User> result = findByQuery(query, email);
            if (result.size() != 0) {
                return result.get(0);
            } else {
                logger.trace("Thar user doesn't exist");
                return null;
            }
        } catch (HibernateException e) {
            logger.error("Find by email user exception " + e, e);
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public User findByIdUser(Long id) {
        logger.trace("Find by id: " + id);
        try {
            return (User) findById(User.class, id);
        } catch (HibernateException e) {
            logger.error("Find by id exception", e, e);
            throw e;
        }
    }

}
