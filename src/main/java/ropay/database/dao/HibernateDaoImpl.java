package ropay.database.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ropay.database.interfaces.HibernateDao;

import java.util.List;

public class HibernateDaoImpl implements HibernateDao {

    protected Logger logger = LoggerFactory.getLogger(HibernateDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public void insert(Object obj) {
        logger.trace("Insert record: " + obj);
        getSession().save(obj);

    }

    @Override
    @Transactional(readOnly = true)
    public Object findById(Class clazz, Long key) {
        logger.trace("Find by id: " + key);
        return getSession().get(clazz, key);
    }

    @Override
    @Transactional(readOnly = true)
    public List findByQuery(String query, String key) {
        logger.trace("Find by query: " + query);
        Query hibernateQuery = getSession().createQuery(query);
        hibernateQuery.setParameter("key", key);
        return hibernateQuery.list();
    }

    @Override
    @Transactional(readOnly = true)
    public List findAllRecords(String query) {
        logger.trace("Find all query: " + query);
        return getSession().createQuery(query).list();
    }


    @Override
    @Transactional
    public void updateObject(Object obj) {
        logger.trace("Update object" + obj);
        getSession().merge(obj);
    }

    @Override
    @Transactional
    public void deleteObject(Object obj) {
        logger.trace("Delete object: " + obj);
        getSession().delete(obj);

    }
}
