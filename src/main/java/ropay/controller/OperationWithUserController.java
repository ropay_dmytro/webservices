package ropay.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ropay.controller.validator.PasswordValidator;
import ropay.controller.validator.VerifyReCaptcha;
import ropay.database.interfaces.UserDao;
import ropay.database.objects.Role;
import ropay.database.objects.User;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/opWithUser")
public class OperationWithUserController {

    private Logger logger = LoggerFactory.getLogger(OperationWithUserController.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private VerifyReCaptcha verifyReCaptcha;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.addValidators(new PasswordValidator());
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegistrationPage(Model model) {
        logger.trace("Show registration page");
        User user = new User();
        user.setRole(new Role());
        model.addAttribute("user", user);
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registrationUser(@Valid @ModelAttribute("user") User user,
                                   BindingResult bindingResult, Model model,
                                   @RequestParam("g-recaptcha-response") String response, Errors errors) {
        logger.trace("Registration user");

        if (bindingResult.hasErrors() || !verifyReCaptcha.verify(response, model)
                || existingLogin(user.getLogin(), errors) || existingEmail(user.getEmail(), errors)) {
            logger.trace("Registration form invalid");
            model.addAttribute("user", user);
            return "registration";
        }

        user.setRole(new Role(2L, "user"));
        logger.trace("Create user: " + user);
        userDao.create(user);
        return "redirect:/login?successful";
    }

    @RequestMapping(value = "/admin/add", method = RequestMethod.GET)
    public String addUserPage(Model model) {
        logger.trace("Show add user page");
        User user = new User();
        user.setRole(new Role());
        model.addAttribute("user", user);
        model.addAttribute("action", "add");
        return "action_with_user";
    }

    @RequestMapping(value = "/admin/add", method = RequestMethod.POST)
    public String addUser(@Valid @ModelAttribute("user") User user,
                          BindingResult bindingResult, Model model, Errors errors) {
        logger.trace("Add user");

        if (bindingResult.hasErrors() || existingLogin(user.getLogin(), errors) || existingEmail(user.getEmail(), errors)) {
            logger.trace("Validation error, form has errors!");
            model.addAttribute("user", user);
            model.addAttribute("action", "add");
            return "action_with_user";
        }

        logger.trace("Add user: " + user);
        userDao.create(user);
        return "redirect:/userList";
    }

    @RequestMapping(value = "/admin/edit-{userId}", method = RequestMethod.GET)
    public String editUserPage(@PathVariable Long userId, Model model) {
        logger.trace("Show edit user page");
        User editUser = userDao.findByIdUser(userId);
        model.addAttribute("user", editUser);
        model.addAttribute("action", "edit");
        return "action_with_user";
    }

    @RequestMapping(value = "/admin/edit-{userId}", method = RequestMethod.POST)
    public String editUser(@Valid @ModelAttribute("user") User user, BindingResult bindingResult,
                           @PathVariable Long userId, Model model, Errors errors) {
        logger.trace("Edit user");

        if (bindingResult.hasErrors() || checkEmailForUpdate(user, errors)) {
            logger.trace("Validation error, data has errors!");
            model.addAttribute("user", user);
            model.addAttribute("action", "edit");
            return "action_with_user";
        }

        logger.trace("Edit user: " + user);
        userDao.update(user);
        return "redirect:/userList";
    }

    @RequestMapping(value = "/admin/delete/{userId}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable Long userId) {
        logger.trace("Delete user where id: " + userId);
        User deleteUser = userDao.findByIdUser(userId);

        if (deleteUser == null) {
            logger.trace("User not exist");
            return "redirect:/userList";
        }
        userDao.remove(deleteUser);
        return "redirect:/userList";
    }

    private boolean existingLogin(String login, Errors errors) {
        User user = userDao.findByLogin(login);
        if (user == null) {
            return false;
        } else {
            errors.rejectValue("login", null, "Try to use another login");
            return true;
        }
    }

    private boolean existingEmail(String email, Errors errors) {
        User user = userDao.findByEmail(email);
        if (user == null) {
            return false;
        } else {
            errors.rejectValue("email", null, "Try to use another email");
            return true;
        }
    }

    private boolean checkEmailForUpdate(User user, Errors errors) {
        User emailUser = userDao.findByEmail(user.getEmail());
        if (emailUser != null && !emailUser.getLogin().equals(user.getLogin())) {
            errors.rejectValue("email", null, "Try to use another email");
            return true;
        } else {
            return false;
        }
    }
}
