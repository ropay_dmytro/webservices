package ropay.controller.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;

@Service("verifyReCaptcha")
public class VerifyReCaptcha {

    private static Logger logger = LoggerFactory.getLogger(VerifyReCaptcha.class);

    private final String url = "https://www.google.com/recaptcha/api/siteverify";
    private final String secret = "6Lc0GG0UAAAAAGMwwpNUhIAADne84W-5YvZcQ6LF";
    private final String userAgent = "Mozilla/61";

    public boolean verify(String gRecaptchaResponse, Model model) {
        logger.trace("Verify captcha response");

        if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
            model.addAttribute("message", "Invalid captcha");
            logger.trace("Invalid captcha response");
            return false;
        }

        try {
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", userAgent);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String postParams = "secret=" + secret + "&response=" + gRecaptchaResponse;

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postParams);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            logger.trace("Sending 'POST' request to URL : " + url);
            logger.trace("Post parameters : " + postParams);
            logger.trace("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            logger.trace(response.toString());

            JsonReader jsonReader = Json.createReader(new StringReader(response.toString()));
            JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();

            return jsonObject.getBoolean("success");
        } catch (Exception e) {
            logger.error("Exception verify captcha!", e, e);
            return false;
        }
    }
}
