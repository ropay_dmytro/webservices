package ropay.database.dao;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ropay.database.interfaces.RoleDao;
import ropay.database.objects.Role;

import java.util.List;

@Repository("roleDao")
public class RoleDaoImpl extends HibernateDaoImpl implements RoleDao {

    private Logger logger = LoggerFactory.getLogger(RoleDaoImpl.class);

    @Override
    @Transactional
    public void create(Role role) {
        logger.trace("Create user" + role);
        Role newRole = findByName(role.getName());
        if (newRole == null) {
            try {
                insert(role);
            } catch (HibernateException e) {
                logger.error("Create role exception " + e, e);
                throw e;
            }
        } else {
            logger.error("That role exist");
            throw new IllegalArgumentException("That role exist");
        }
    }

    @Override
    @Transactional
    public void update(Role role) {
        logger.trace("Update user" + role);
        Role newRole = (Role) findById(Role.class, role.getId());
        if (newRole != null) {
            role.setId(newRole.getId());
            try {
                updateObject(role);
            } catch (HibernateException e) {
                logger.error("Update role exception" + e, e);
                throw e;
            }
        } else {
            logger.error("That role doesn't exist");
            throw new IllegalArgumentException("That role doesn't exist");
        }
    }

    @Override
    @Transactional
    public void remove(Role role) {
        logger.trace("Remove user" + role);
        Role newRole = findByName(role.getName());
        if (newRole != null) {
            try {
                deleteObject(newRole);
            } catch (HibernateException e) {
                logger.error("Delete role exception" + e, e);
                throw e;
            }

        } else {
            logger.error("That role doesn't exist");
            throw new IllegalArgumentException("That role doesn't exist");
        }
    }

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public Role findByName(String name) {
        logger.trace("Find by name: " + name);
        String query = "from Role where name = :key";
        try {
            List<Role> result = findByQuery(query, name);
            if (result.size() != 0) {
                return result.get(0);
            } else {
                logger.trace("That role doesn't exist");
                return null;
            }
        } catch (HibernateException e) {
            logger.error("Find by name role exception" + e, e);
            throw e;
        }
    }


}
