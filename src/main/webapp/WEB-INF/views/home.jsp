<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html lang="en">
<head>
    <link rel="stylesheet" href="<c:url value="/resources/style.css"/>">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Home</title>
</head>
<body>
<c:url value="/logout" var="logoutUrl"/>
<form action="${logoutUrl}" method="post" id="logoutForm">
</form>

<security:authorize access="hasRole('ROLE_USER')">
    <div id="main-heading-user">
        <h1 style="text-align: center">Hello <security:authentication property="principal.username"/></h1>
        <h3 style="text-align: center">Click <a href="javascript:formSubmit()">here</a> to logout.</h3>
    </div>

</security:authorize>

<security:authorize access="hasRole('ROLE_ADMIN')">

    <div id="admin_logout">Admin <security:authentication property="principal.username"/> (<a
            href="javascript:formSubmit()"><u>Logout</u></a>)
    </div>

    <div id="add_user"><a href="opWithUser/admin/add">Add new user</a></div>
    <div class="main-table">
        <table>
            <tr>
                <th>Login</th>
                <th>First name</th>
                <th>Last name</th>
                <th>Age</th>
                <th>Role</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="user" items="${userList}" varStatus="status">
            <tr>
                    <td>${user.login}</td>
                    <td>${user.firstName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.age}</td>
                    <td>${user.role.name}</td>
                    <td>
                        <a href="opWithUser/admin/edit-${user.id}">Edit</a>
                        <a href="opWithUser/admin/delete/${user.id}"
                           onclick="return confirm('Delete ${user.login}?');">Delete</a>
                    </td>
            </tr>
            </c:forEach>
        </table>
    </div>
</security:authorize>

<script>
    function formSubmit() {
        document.getElementById("logoutForm").submit();
    }
</script>
</body>
</html>