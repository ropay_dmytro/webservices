package ropay.database.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import ropay.database.interfaces.RoleDao;
import ropay.database.interfaces.UserDao;
import ropay.database.objects.Role;
import ropay.database.objects.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-hibernate.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TestHibernateUserDao {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Test
    @DatabaseSetup("classpath:UserTestDataSet.xml")
    @ExpectedDatabase(assertionMode= DatabaseAssertionMode.NON_STRICT,
            value = "classpath:expected_table_for_user/insertuser.xml")
    public void testCreateNewUserMustWorkCorrect() throws Exception {
        userDao.create(new User("zoo", "123", "zoo@gmail.com", "mark", "markov",
                LocalDate.parse("1998-12-12"), new Role(2L)));
    }

    @Test
    @DatabaseSetup("classpath:UserTestDataSet.xml")
    public void testFindByLoginMustReturnCorrectUser() {
        User expected = new User(1L, "foo", "123", "foo@gmail.com", "bob",
                "bobovich", LocalDate.parse("1998-11-11"), new Role(1L, "manager"));
        User actual = userDao.findByLogin("foo");
        assertEquals(expected, actual);
    }

    @Test
    @DatabaseSetup("classpath:RoleTestDataSet.xml")
    @DatabaseSetup("classpath:UserTestDataSet.xml")
    public void testFindByEmailMustReturnCorrectUser() {
        User expected = new User(1L, "foo", "123", "foo@gmail.com", "bob",
                "bobovich", LocalDate.parse("1998-11-11"), new Role(1L, "manager"));
        User actual = userDao.findByEmail("foo@gmail.com");
        assertEquals(expected, actual);
    }

    @Test
    @DatabaseSetup("classpath:UserTestDataSet.xml")
    public void testFindAllMustFindAllUsers() {
        List<User> expected = new ArrayList<>();
        expected.add(new User(1L, "foo", "123", "foo@gmail.com", "bob",
                "bobovich", LocalDate.parse("1998-11-11"), new Role(1L, "manager")));
        expected.add(new User(2L, "bar", "123", "bar@gmail.com", "alice",
                "alicevna", LocalDate.parse("1998-12-12"), new Role(2L, "programmer")));
        List<User> actual = userDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    @DatabaseSetup("classpath:UserTestDataSet.xml")
    @ExpectedDatabase(assertionMode= DatabaseAssertionMode.NON_STRICT,
            value = "classpath:expected_table_for_user/updateuser.xml")
    public void testUpdateUserMustWorkRight() throws Exception {
        userDao.update(new User(1L, "foo", "123", "foonew@gmail.com", "bobnew",
                "bobovich", LocalDate.parse("1998-11-11"), new Role(1L, "manager")));

    }

    @Test
    @DatabaseSetup("classpath:UserTestDataSet.xml")
    @ExpectedDatabase(assertionMode= DatabaseAssertionMode.NON_STRICT, value =  "classpath:expected_table_for_user/deleteuser.xml")
    public void testRemoveUserMustWorkRight() throws Exception {
        userDao.remove(new User(1L, "foo", "123", "foo@gmail.com", "bob",
                "bobovich", LocalDate.parse("1998-11-11"),new Role(1L, "manager")));
    }

}
