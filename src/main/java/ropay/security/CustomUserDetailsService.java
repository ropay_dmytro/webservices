package ropay.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ropay.database.interfaces.UserDao;
import ropay.database.objects.User;

import java.util.Arrays;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        logger.trace("Find by username: " + s);
        User user = userDao.findByLogin(s);
        if (user == null) {
            logger.trace("User not found!");
        }
        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + user.getRole().getName().toUpperCase());
        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(),
                Arrays.asList(authority));
    }
}

