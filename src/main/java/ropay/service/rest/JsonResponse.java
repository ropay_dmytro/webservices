package ropay.service.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JsonResponse {

    private int status;

    private String type;

    private String message;

    public JsonResponse() {
    }

    public JsonResponse(int status, String type, String message) {
        this.status = status;
        this.type = type;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "JsonResponse{" +
                "status=" + status +
                ", type='" + type + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
