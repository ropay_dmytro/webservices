package ropay.service.rest.validation;


import ropay.service.rest.JsonResponse;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<javax.validation.ValidationException> {

    @Override
    public Response toResponse(ValidationException exception) {
        List<JsonResponse> errorList = new ArrayList<>();
        for (ConstraintViolation<?> cv : ((ConstraintViolationException) exception).getConstraintViolations()) {
            errorList.add(new JsonResponse(400, "Validation error",
                    cv.getPropertyPath().toString() + ": " + cv.getMessage()));
        }

        return Response
                .status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON)
                .entity(new GenericEntity<List<JsonResponse>>(errorList){})
                .build();
    }
}
