package ropay.service.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ropay.database.interfaces.RoleDao;
import ropay.database.interfaces.UserDao;
import ropay.database.objects.User;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/")
@Component
public class UserRestService {

    private Logger logger = LoggerFactory.getLogger(UserRestService.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public GenericEntity<List<User>> getUsers() {
        logger.trace("Get all users");
        return new GenericEntity<List<User>>(userDao.findAll()) {
        };
    }

    @GET
    @Path("/login/{login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserByLogin(@PathParam("login") String login) {
        User user = userDao.findByLogin(login);
        logger.trace("Get user by login: " + login);
        if (user == null) {
            logger.trace("User not found");
            return Response.status(400).entity(new JsonResponse(400, "Error", "User not found")).build();
        }

        return Response.status(200).entity(user).build();
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(@Valid User user) throws ValidationException {
        logger.trace("Create user: " + user.getLogin());
        if (userDao.findByLogin(user.getLogin()) != null) {
            logger.trace("User exist. need use another login");
            return Response.status(409).entity(new JsonResponse(409, "Error", "Try to use another login")).build();
        }

        if (doPasswordsNotMatch(user)) {
            logger.trace("Passwords don't match");
            return Response.status(400).entity(new JsonResponse(400, "Error", "Passwords don't match")).build();
        }

        if (userDao.findByEmail(user.getEmail()) != null) {
            logger.trace("Should use another email");
            return Response.status(409).entity(new JsonResponse(409, "Error", "Try to use another email")).build();
        }

        if (doesRoleNotExist(user.getRole().getName())) {
            logger.trace("Role doesn't exist");
            return Response.status(400).entity(new JsonResponse(400, "Error", "Role not found")).build();
        }

        user.getRole().setId(user.getRole().getName().equals("admin") ? 1L : 2L);
        userDao.create(user);
        return Response.status(201).entity(new JsonResponse(201, "Successful", "User created")).build();
    }

    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(@Valid User user) {
        User checkUser = userDao.findByEmail(user.getEmail());
        logger.trace("Update user: " + user.getLogin());

        if (userDao.findByLogin(user.getLogin()) == null) {
            logger.trace("User not found");
            return Response.status(400).entity(new JsonResponse(400, "Error", "User not found")).build();
        }

        if (doPasswordsNotMatch(user)) {
            logger.trace("Passwords don't match");
            return Response.status(400).entity(new JsonResponse(400, "Error", "Passwords don't match")).build();
        }

        if (checkUser != null && !checkUser.getLogin().equals(user.getLogin())) {
            logger.trace("Should use another email");
            return Response.status(409).entity(new JsonResponse(409, "Error", "Try to use another email")).build();
        }

        if (doesRoleNotExist(user.getRole().getName())) {
            logger.trace("Role doesn't exist");
            return Response.status(400).entity(new JsonResponse(400, "Error", "Role not found")).build();
        }
        user.getRole().setId(user.getRole().getName().equals("admin") ? 1L : 2L);
        userDao.update(user);
        return Response.status(201).entity(new JsonResponse(201, "Successful", "User updated")).build();
    }

    @DELETE
    @Path("/delete/{login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("login") String login) {
        User user = userDao.findByLogin(login);
        logger.trace("Delete user");
        if (user == null) {
            logger.trace("User not found");
            return Response.status(400).entity(new JsonResponse(400, "Error", "User not found")).build();
        }
        userDao.remove(user);
        return Response.status(200).entity(new JsonResponse(200, "Successful", "User deleted")).build();
    }

    private boolean doPasswordsNotMatch(User user) {
        return !user.getPassword().equals(user.getConfirmPassword());
    }

    private boolean doesRoleNotExist(String roleName) {
        return roleDao.findByName(roleName) == null;
    }
}
