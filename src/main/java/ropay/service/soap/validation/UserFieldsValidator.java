package ropay.service.soap.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ropay.database.objects.User;

import java.util.ArrayList;
import java.util.List;

public class UserFieldsValidator {

    private Logger logger = LoggerFactory.getLogger(UserFieldsValidator.class);

    public List<String> validate(User user) {

        List<String> errorList = new ArrayList<>();

        logger.trace("Start validation");

        if (user.getLogin().length() <= 2 || user.getLogin().length() >= 70) {
            logger.trace("Invalid size of login");
            errorList.add("Size must be > 2 and < 70");
        }

        if (!user.getLogin().matches("^[a-z\\d][a-z\\d]*[_-]?[a-z\\d]*[a-z\\d]$")) {
            logger.trace("Invalid login");
            errorList.add("Invalid login");
        }

        if (user.getPassword().trim().length() == 0) {
            logger.trace("Invalid password");
            errorList.add("Enter password");
        }

        if (user.getPassword().length() <= 2 || user.getPassword().length() >= 80) {
            logger.trace("Invalid size of password");
            errorList.add("Size must be > 2 and < 80");
        }

        if (!user.getConfirmPassword().equals(user.getPassword())) {
            logger.trace("Passwords don't match");
            errorList.add("Passwords don't match");
        }

        if (!user.getEmail().matches("^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$")) {
            logger.trace("Invalid email");
            errorList.add("Invalid email");
        }

        if (!user.getFirstName().matches("^[a-zA-Z\\d][a-zA-Z\\d]*[_-]?[a-zA-Z\\d]*[a-zA-Z\\d]$")) {
            logger.trace("Invalid first name");
            errorList.add("Invalid name");
        }

        if (user.getFirstName().length() <= 2 || user.getFirstName().length() >= 20) {
            logger.trace("Invalid first name");
            errorList.add("Size must be > 2 and < 20");
        }

        if (user.getLastName().length() <= 2 || user.getLastName().length() >= 20 ||
                !user.getLastName().matches("^[a-zA-Z\\d][a-zA-Z\\d]*[_-]?[a-zA-Z\\d]*[a-zA-Z\\d]$")) {
            logger.trace("Invalid last name");
            errorList.add("Invalid last name");
        }

        if (user.getLastName().length() <= 2 || user.getLastName().length() >= 20) {
            logger.trace("Invalid last name");
            errorList.add("Size must be > 2 and < 20");
        }

        if (user.getBirthday() == null) {
            logger.trace("Invalid date");
            errorList.add("Enter birthday");
        }

        if (user.getBirthday() != null && user.getAge() <= 0) {
            logger.trace("Invalid date");
            errorList.add("Only past date is valid");
        }

        return errorList;
    }

}
