package ropay.service.soap;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;


import org.apache.cxf.jaxws.EndpointImpl;

import javax.xml.ws.Endpoint;

@Configuration
@ComponentScan
@ImportResource({ "classpath:META-INF/cxf/cxf.xml" })
@PropertySource("classpath:app-env.properties")
@PropertySource("classpath:app.properties")
public class SoapConfig {

    private Logger logger = LoggerFactory.getLogger(SoapConfig.class);

    @Autowired
    private UserService userService;

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), userService);;
        endpoint.setAddress("/user");
        endpoint.setWsdlLocation("classpath:CxfService/CxfService-v1.0.wsdl");
        endpoint.publish();
        return endpoint;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
