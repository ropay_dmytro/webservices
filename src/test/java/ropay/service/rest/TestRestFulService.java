package ropay.service.rest;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ropay.database.objects.Role;
import ropay.database.objects.User;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestRestFulService {

    private WebTarget webTarget;

    public TestRestFulService() {
        webTarget = ClientBuilder.newClient(new ClientConfig()).target("http://10.10.103.163:8080/education/rest/");
    }

    @Before
    public void createUser(){
        User newUser = new User("zoo", "123", "zoo@zoo.com", "mark"
                , "markov", LocalDate.parse("1998-12-12"), new Role(2L, "user"));
        newUser.setConfirmPassword("123");
        webTarget.path("create").request().post(Entity.entity(newUser, MediaType.APPLICATION_JSON)).readEntity(String.class);
    }

    @After
    public void deleteUser(){
        webTarget.path("delete/zoo").request().delete();
    }

    @Test
    public void testCreateUserMustWorkCorrect() {

        User newUser = new User("roo", "123", "roo@roo.com", "mark"
                , "markov", LocalDate.parse("1998-12-12"), new Role(2L, "user"));
        newUser.setConfirmPassword("123");

        Response response = webTarget.path("create").request().post(Entity.entity(newUser, MediaType.APPLICATION_JSON));

        assertEquals(201, response.getStatus());
        assertEquals(new JsonResponse(201, "Successful", "User created").toString(),
                response.readEntity(JsonResponse.class).toString());

        assertTrue(webTarget.path("users").request(MediaType.APPLICATION_JSON).get().readEntity(new GenericType<List<User>>() {}).contains(newUser));

        assertEquals(new JsonResponse(200, "Successful", "User deleted").toString()
                , webTarget.path("delete/" + newUser.getLogin()).request().delete().readEntity(JsonResponse.class).toString());
    }

    @Test
    public void testCreateExistUserMustReturnConflictStatus() {
        User newUser = new User("zoo", "123", "zoo@zoo.com", "mark"
                , "markov", LocalDate.parse("1998-12-12"), new Role(2L, "user"));
        Response response = webTarget.path("create").request().post(Entity.entity(newUser, MediaType.APPLICATION_JSON));

        assertEquals(409, response.getStatus());
        assertEquals(new JsonResponse(409, "Error", "Try to use another login").toString(),
                response.readEntity(JsonResponse.class).toString());
    }

    @Test
    public void testCreateUserWithExistEmailMustReturnConflictStatus() {
        User newUser = new User("roo", "123", "zoo@zoo.com", "mark"
                , "markov", LocalDate.parse("1998-12-12"), new Role(2L, "user"));
        newUser.setConfirmPassword("123");
        Response response = webTarget.path("create").request().post(Entity.entity(newUser, MediaType.APPLICATION_JSON));

        assertEquals(409, response.getStatus());
        assertEquals(new JsonResponse(409, "Error", "Try to use another email").toString(),
                response.readEntity(JsonResponse.class).toString());
    }

    @Test
    public void testDeleteUserMustWorkCorrect(){
        Response response = webTarget.path("delete/zoo").request().delete();
        assertEquals(200, response.getStatus());
        assertEquals(new JsonResponse(200, "Successful", "User deleted").toString(),
                response.readEntity(JsonResponse.class).toString());

        response = webTarget.path("login/zoo").request().get();
        assertEquals(400, response.getStatus());
        assertEquals(new JsonResponse(400, "Error", "User not found").toString(),
                response.readEntity(JsonResponse.class).toString());
        assertFalse(webTarget.path("users").request().get().readEntity(new GenericType<List<User>>() {})
                .contains(new User("zoo", "123", "zoo@zoo.com", "mark"
                        , "markov", LocalDate.parse("1998-12-12"), new Role(2L, "user"))));
    }

    @Test
    public void testDeleteNonExistUserMustReturnConflictStatus(){

        Response response = webTarget.path("delete/roo").request().delete();
        assertEquals(400, response.getStatus());
        assertEquals(new JsonResponse(400, "Error", "User not found").toString(),
                response.readEntity(JsonResponse.class).toString());
    }

    @Test
    public void testUpdateUserMustWorkCorrect() {

        User newUser = new User("zoo", "123", "zoo@zoo.com", "newname"
                , "newlastname", LocalDate.parse("1998-12-12"), new Role(2L, "user"));
        newUser.setConfirmPassword("123");
        Response response = webTarget.path("update").request().put(Entity.entity(newUser, MediaType.APPLICATION_JSON));

        assertEquals(201, response.getStatus());
        assertEquals(new JsonResponse(201, "Successful", "User updated").toString(),
                response.readEntity(JsonResponse.class).toString());
        assertEquals("newlastname", webTarget.path("login/" +
                newUser.getLogin()).request().get().readEntity(User.class).getLastName());
    }

    @Test
    public void testUpdateUserWithExistEmailMustReturnConflictStatus() {

        User newUser = new User("zoo", "123", "foo@foo.com", "newName"
                , "newLastName", LocalDate.parse("1998-12-12"), new Role(2L, "user"));
        newUser.setConfirmPassword("123");
        Response response = webTarget.path("update").request().put(Entity.entity(newUser, MediaType.APPLICATION_JSON));

        assertEquals(409, response.getStatus());
        assertEquals(new JsonResponse(409, "Error", "Try to use another email").toString(),
                response.readEntity(JsonResponse.class).toString());
    }

}
