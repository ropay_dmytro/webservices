package ropay.database.interfaces;

import ropay.database.objects.User;

import java.util.List;

public interface UserDao {

    void create(User user);

    void update(User user);

    void remove(User user);

    List<User> findAll();

    User findByLogin(String login);

    User findByEmail(String email);

    User findByIdUser(Long userId);
}
