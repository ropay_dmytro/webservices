package ropay.database.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import ropay.database.interfaces.RoleDao;
import ropay.database.objects.Role;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-hibernate.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TestHibernateRoleDao {

    @Autowired
    private RoleDao roleDao;


    @Test
    @DatabaseSetup("classpath:RoleTestDataSet.xml")
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "classpath:expected_table_for_role/role.xml")
    public void testCreateRoleMustWorkCorrect() throws Exception {

        roleDao.create(new Role("HR"));
    }

    @Test
    @DatabaseSetup("classpath:RoleTestDataSet.xml")
    public void testFindByNameMustReturnRightRole() {
        Role expected = new Role("manager");
        Role actual = roleDao.findByName("manager");
        assertNotNull(actual);
        assertEquals(expected, actual);
    }

    @Test
    @DatabaseSetup("classpath:RoleTestDataSet.xml")
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "classpath:expected_table_for_role/updaterole.xml")
    public void testUpdateMustWorkCorrect() throws Exception {
        roleDao.update(new Role(2L, "middle_programmer"));
    }

    @Test
    @DatabaseSetup("classpath:RoleTestDataSet.xml")
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "classpath:expected_table_for_role/deleterole.xml")
    public void testDeleteRoleMustWorkCorrect() throws Exception {
        roleDao.remove(new Role("programmer"));
    }
}
