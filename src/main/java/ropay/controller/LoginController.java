package ropay.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping(value = {"/"})
public class LoginController {

    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping(value = {"/", "/login"}, method = GET)
    public String showLoginForm() {
        logger.trace("Show login form");
        return "login";
    }

    @RequestMapping(value = "/list", method = GET)
    public String authenticationUser() {
        return "redirect:/userList";
    }

}
