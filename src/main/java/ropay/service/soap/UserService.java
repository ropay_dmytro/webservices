package ropay.service.soap;

import ropay.database.objects.User;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://service.ropay/soap/generate")
public interface UserService {

   @WebResult(name = "user", targetNamespace = "http://service.ropay/soap/generate", partName = "user")
   @WebMethod(action = "http://service.ropay/soap/generate")
   User getUser(String login);

   @WebMethod
   String createUser(User user);
}
