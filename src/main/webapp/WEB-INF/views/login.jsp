<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html;
    charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <link rel="stylesheet" href="<c:url value="/resources/style.css"/>">

    <title>Login</title>
</head>
<body>

<div id="modal">
    <form id="login_form" action="<spring:url value="/loginAction"/> " method="post">
        <div>
            <label>Login</label>
            <input type="text" name="login" id="user-login" placeholder="Login" value="${login}">
        </div>
        <div>
            <label>Password</label>
            <input type="password" name="password" id="user-password" placeholder="Password">
        </div>
        <input type="submit" value="Sign in">
        <input type="button" value="Registration" onclick="location.href='/education/opWithUser/registration'"
               style="
                background-color: skyblue;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                color: white;
                padding: 8px 8px;
                margin: 8px 0;"/><br>
        <label style="color: red"><c:if test="${param.error != null}">Invalid login of password!</c:if></label>
        <label style="color: green"><c:if test="${param.successful != null}">Registration successful.</c:if></label>
    </form>
</div>

</body>
</html>