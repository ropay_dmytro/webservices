package ropay.controller.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateConverter implements Converter<String, LocalDate> {

    private Logger logger = LoggerFactory.getLogger(LocalDateConverter.class);

    private final DateTimeFormatter formatter;

    public LocalDateConverter(String dateFormat) {
        this.formatter = DateTimeFormatter.ofPattern(dateFormat);
    }

    @Override
    public LocalDate convert(String source) {
        logger.trace("Convert: " + source);
        if (source == null || source.isEmpty() || !source.matches("^\\d{4}-\\d{2}-\\d{2}$")) {
            logger.trace("Invalid source!");
            return null;
        }
        try {
            return LocalDate.parse(source, formatter);
        } catch (Exception e) {
            logger.trace("Invalid source! Cant parse in date");
            return null;
        }
    }
}
