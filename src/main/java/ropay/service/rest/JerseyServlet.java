package ropay.service.rest;

import com.sun.jersey.spi.spring.container.servlet.SpringServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = {"/rest/*"}, initParams = {
        @WebInitParam(name = "com.sun.jersey.config.property.packages",
                value = "ropay.service.rest")})
public class JerseyServlet extends SpringServlet {
}
