package ropay.database.interfaces;

import java.util.List;

public interface HibernateDao {

    void insert(Object obj);

    Object findById(Class clazz, Long key);

    List findByQuery(String query, String key);

    List findAllRecords(String query);

    void updateObject(Object obj);

    void deleteObject(Object obj);
}
