package ropay.service.soap.validation;

import org.springframework.ws.soap.server.endpoint.interceptor.PayloadValidatingInterceptor;

import javax.xml.namespace.QName;


public class CustomValidationInterceptor extends PayloadValidatingInterceptor {

    @Override
    public QName getDetailElementName() {
        return new QName("http://service.ropay/soap/generate", "error", "my");
    }

}
