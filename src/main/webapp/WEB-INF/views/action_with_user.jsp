<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="<c:url value="/resources/style.css"/>">
    <c:if test="${action=='add'}">
        <title>Add user</title>

    </c:if>
    <c:if test="${action=='edit'}">
        <title>Edit user</title>

    </c:if>
</head>
<body>
<div id="add_user_text">
    <c:if test="${action=='add'}">
        <h1>Add user</h1>
    </c:if>
    <c:if test="${action=='edit'}">
        <h1>Edit user</h1>
    </c:if>
</div>
<div id="modal_add_user">
    <form:form method="post" modelAttribute="user">
        <table>
            <tr>
                <td>Login</td>
                <td>
                    <c:if test="${action=='add'}">
                        <form:input path="login"/>
                        <form:errors path="login" cssClass="error"/>
                    </c:if>
                    <c:if test="${action=='edit'}">
                        <input type="text" name="login" value="${user.login}" readonly>
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td>
                    <form:password path="password"/>
                    <form:errors path="password" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>Repeat password</td>
                <td>
                    <form:password path="confirmPassword"/>
                    <form:errors path="confirmPassword" cssClass="error"/>
                </td>

            <tr>
                <td>Email</td>
                <td>
                    <form:input path="email"/>
                    <form:errors path="email" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>First name</td>
                <td>
                    <form:input path="firstName"/>
                    <form:errors path="firstName" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>Last name</td>
                <td>
                    <form:input path="lastName"/>
                    <form:errors path="lastName" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>
                    Birthday<br>
                    (yyyy-MM-dd)
                </td>
                <td>
                    <form:input path="birthday"/>
                    <form:errors path="birthday" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>Role</td>
                <td><label>
                    <form:select path="role.id">
                        <form:option value="1">admin</form:option>
                        <form:option value="2">user</form:option>
                    </form:select>
                </label></td>
            </tr>
            <tr>
                <td colspan="2"><label class="error">${message}</label>
                <td>
            </tr>
            <tr>
                <td><input type="submit" value="Ok"></td>
                <td><input type="button" value="Cancel" onclick="location.href='/education/userList'"/></td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
