package ropay.database.objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;
import ropay.service.rest.sealizer.LocalDateDeserializer;
import ropay.service.rest.sealizer.LocalDateSerializer;
import ropay.service.soap.adapter.DateAdapter;

import javax.persistence.*;
import javax.validation.constraints.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

@Entity
@Table(name = "USER")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "User", propOrder = {
        "login",
        "password",
        "confirmPassword",
        "email",
        "firstName",
        "lastName",
        "birthday",
        "role"
}, namespace = "http://service.ropay/soap/generate")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @XmlTransient
    private Long id;

    @NotNull
    @Size(min = 3, max = 16, message = "Size must be more 3 and less 16")
    @Column(name = "login")
    @NotBlank(message = "Enter login")
    @Pattern(regexp = "^[a-z\\d][a-z\\d]*[_-]?[a-z\\d]*[a-z\\d]$", message = "Invalid login")
    private String login;

    @NotNull
    @Size(min = 3, max = 80, message = "Size must be more 3 and less 80")
    @Column(name = "password")
    @NotBlank(message = "Enter password")
    private String password;

    @Transient
    private String confirmPassword;

    @NotNull
    @Pattern(regexp = "^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$", message = "Invalid email")
    @Column(name = "email")
    private String email;

    @Size(min = 3, max = 16, message = "Size must be more 3 and less 16")
    @Column(name = "first_name")
    @NotBlank(message = "Enter first name")
    @Pattern(regexp = "^[a-zA-Z\\d][a-zA-Z\\d]*[_-]?[a-zA-Z\\d]*[a-zA-Z\\d]$", message = "Invalid name")
    private String firstName;

    @Size(min = 3, max = 16, message = "Size must be more 3 and less 16")
    @Column(name = "last_name")
    @NotBlank(message = "Enter last name")
    @Pattern(regexp = "^[a-zA-Z\\d][a-zA-Z\\d]*[_-]?[a-zA-Z\\d]*[a-zA-Z\\d]$", message = "Invalid last name")
    private String lastName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Past(message = "Only the past is valid")
    @NotNull(message = "Data is not valid!")
    @Column(name = "birthday")
    @XmlJavaTypeAdapter(value = DateAdapter.class)
    private LocalDate birthday;

    @ManyToOne
    @JoinColumn(name = "role")
    private Role role;

    public User() {
    }

    public User(String login, String password, String email, String firstName, String lastName, LocalDate birthday, Role role) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.role = role;
    }

    public User(Long id, String login, String password, String email, String firstName, String lastName, LocalDate birthday, Role role) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.role = role;
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

//    @XmlSchemaType(name = "date")
    public LocalDate getBirthday() {
        return birthday;
    }

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @JsonIgnore
    public int getAge() {
        return Period.between(getBirthday(), LocalDate.now()).getYears();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(login, user.login) &&
                Objects.equals(email, user.email) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(birthday, user.birthday) &&
                Objects.equals(role, user.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, email, firstName, lastName, birthday, role);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", role=" + role +
                '}';
    }

}
