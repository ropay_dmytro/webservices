package ropay.service.rest;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("com.sun.jersey.api.json.POJOMappingFeature", "true");
        setProperties(propertyMap);
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);
    }
}

