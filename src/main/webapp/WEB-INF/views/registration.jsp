<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <link rel="stylesheet" href="<c:url value="/resources/style.css"/>">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <title>Registration</title>
</head>
<body>
<h1 id="add_user_text">Registration</h1>
<div id="modal_add_user">
    <form:form method="post" modelAttribute="user">
        <table>
            <tr>
                <td>Login</td>
                <td>
                    <form:input path="login"/>
                    <form:errors path="login" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td>
                    <form:password path="password"/>
                    <form:errors path="password" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>Repeat password</td>
                <td>
                    <form:password path="confirmPassword"/>
                    <form:errors path="confirmPassword" cssClass="error"/>
                </td>

            <tr>
                <td>Email</td>
                <td>
                    <form:input path="email"/>
                    <form:errors path="email" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>First name</td>
                <td>
                    <form:input path="firstName"/>
                    <form:errors path="firstName" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>Last name</td>
                <td>
                    <form:input path="lastName"/>
                    <form:errors path="lastName" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>
                    Birthday<br>
                    (yyyy-MM-dd)
                </td>
                <td>
                    <form:input path="birthday"/>
                    <form:errors path="birthday" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="g-recaptcha" data-sitekey="6Lc0GG0UAAAAAH-LN34h6yRs7CNAFjpZl1xi-t8I"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2"><label class="error">${message}</label>
                <td>
            </tr>
            <tr>
                <td><input type="submit" value="Ok"></td>
                <td><input type="button" value="Cancel" onclick="location.href='/education/login'"/></td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
